using System;
using System.Collections.Generic;

namespace RedTail.Mail
{
  public class MailRequest
    : BaseRequest
  {
    public MailContent Content { get; set; }
    public MailAddress From { get; set; }
    public List<MailPersonalization> Personalizations { get; } = new List<MailPersonalization>();

    public void AddPersonalization(MailPersonalization personalization)
    {
      if (MailConfiguration.Settings.TestMode.TestMode) {
        personalization.To = new List<MailAddress>
        {
          new MailAddress
          {
            Address = MailConfiguration.Settings.TestMode.EmailAddress
          }
        };
      }

      this.Personalizations.Add(personalization);
    }
  }
}
