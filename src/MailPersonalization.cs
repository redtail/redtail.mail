using System;
using System.Collections.Generic;

using SendGrid.Helpers.Mail;

namespace RedTail.Mail
{
  public class MailPersonalization
  {
    public List<MailAddress> Bcc { get; set; } = new List<MailAddress>();
    public List<MailAddress> Cc { get; set; } = new List<MailAddress>();
    public Dictionary<String, String> Headers { get; set; } = new Dictionary<String, String>();
    public String Subject { get; set; }
    public Dictionary<String, String> Substitutions { get; set; } = new Dictionary<String, String>();
    public List<MailAddress> To { get; set; } = new List<MailAddress>();

    public void AddBcc(MailAddress bcc)
    {
      this.Bcc.Add(bcc);
    }

    public void AddCc(MailAddress cc)
    {
      this.Cc.Add(cc);
    }

    public void AddHeader(String key, String value)
    {
      this.Headers.Add(key, value);
    }

    public void AddSubstitution(String key, String value)
    {
      this.Substitutions.Add(key, value);
    }

    public void AddTo(MailAddress to)
    {
      this.To.Add(to);
    }

    public Personalization ToSendGridPersonalization()
    {
      Personalization personalization = new Personalization();

      if (!String.IsNullOrWhiteSpace(this.Subject))
        personalization.Subject = this.Subject;

      if (this.Headers != null)
        personalization.Headers = this.Headers;

      if (this.Substitutions != null)
        personalization.Substitutions = this.Substitutions;

      if (this.To != null && this.To.Count > 0)
      {
        personalization.Tos = new List<EmailAddress>();
        foreach (MailAddress itmMailAddress in this.To)
          personalization.Tos.Add(itmMailAddress.ToSendGridEmailAddress());
      }

      if (this.Cc != null && this.Cc.Count > 0)
      {
        personalization.Ccs = new List<EmailAddress>();
        foreach (MailAddress itmMailAddress in this.Cc)
          personalization.Ccs.Add(itmMailAddress.ToSendGridEmailAddress());
      }

      if (this.Bcc != null && this.Bcc.Count > 0)
      {
        personalization.Bccs = new List<EmailAddress>();
        foreach (MailAddress itmMailAddress in this.Bcc)
          personalization.Bccs.Add(itmMailAddress.ToSendGridEmailAddress());
      }

      return personalization;
    }
  }
}
