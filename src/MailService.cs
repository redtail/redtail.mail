using System;
using System.Collections.Generic;

namespace RedTail.Mail
{
  public class MailService
		: IMailService
	{
		public MailResponse Send(MailRequest request, Boolean stopOnError = true)
		{
			MailHandler handler = new MailHandler(request, stopOnError);
			handler.Execute();

			return handler.GetResponse();
		}

		public MailResponse Send(MailContent content, MailAddress from, MailPersonalization personalization, Boolean stopOnError = true)
		{
			MailRequest request = new MailRequest
			{
				Content = content,
				From = from
			};

      request.AddPersonalization(personalization);

      return this.Send(request, stopOnError);
		}

		public MailResponse Send(MailContent content, MailAddress from, List<MailPersonalization> personalizations, Boolean stopOnError = true)
		{
      MailRequest request = new MailRequest
      {
        Content = content,
        From = from
      };

      foreach (MailPersonalization itmMailPersonalization in personalizations) {
        request.AddPersonalization(itmMailPersonalization);
      }

      return this.Send(request, stopOnError);
		}

		public MailResponse Send(MailContent content, MailAddress from, String subject, MailAddress to, Boolean stopOnError = true)
		{
      MailRequest request = new MailRequest
			{
				Content = content,
				From = from
			};

      request.AddPersonalization(new MailPersonalization
      {
        Subject = subject,
        To = new List<MailAddress> { to }
      });

      return this.Send(request, stopOnError);
		}

		public MailResponse Send(MailContent content, MailAddress from, String subject, List<MailAddress> to, Boolean stopOnError = true)
		{
      MailRequest request = new MailRequest
			{
				Content = content,
				From = from
			};

      request.AddPersonalization(new MailPersonalization
      {
        Subject = subject,
        To = to
      });

      return this.Send(request, stopOnError);
		}

		public MailResponse Send(String body, String fromAddress, String subject, String toAddress, Boolean stopOnError = true)
		{
      MailRequest request = new MailRequest
			{
        Content = new MailContent { Html = body, Text = body },
				From = new MailAddress { Address = fromAddress }
			};

      request.AddPersonalization(new MailPersonalization
      {
        Subject = subject,
        To = new List<MailAddress>
        {
          new MailAddress { Address = toAddress }
        }
      });

      return this.Send(request, stopOnError);
		}

		public MailResponse Send(String body, String fromAddress, String fromName, String subject, String toAddress, Boolean stopOnError = true)
		{
      MailRequest request = new MailRequest
			{
        Content = new MailContent { Html = body, Text = body },
				From = new MailAddress { Address = fromAddress, Name = fromName }
			};

      request.AddPersonalization(new MailPersonalization
      {
        Subject = subject,
        To = new List<MailAddress>
        {
          new MailAddress { Address = toAddress }
        }
      });

      return this.Send(request, stopOnError);
		}

		public MailResponse Send(String body, String fromAddress, String fromName, String subject, String toAddress, String toName, Boolean stopOnError = true)
		{
      MailRequest request = new MailRequest
			{
        Content = new MailContent { Html = body, Text = body },
				From = new MailAddress { Address = fromAddress, Name = fromName }
			};

      request.AddPersonalization(new MailPersonalization
      {
        Subject = subject,
        To = new List<MailAddress>
        {
          new MailAddress { Address = toAddress, Name = toName }
        }
      });

      return this.Send(request, stopOnError);
		}
	}
}
