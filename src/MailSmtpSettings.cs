using System;

namespace RedTail.Mail
{
  public class MailSmtpSettings
  {
    public String Host { get; set; }
    public String Password { get; set; }
    public String Port { get; set; }
    public String UserName { get; set; }
  }
}
