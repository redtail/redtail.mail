using System;

namespace RedTail.Mail
{
  public class MailSendGridSettings
  {
    public String ApiKey { get; set; }
  }
}
