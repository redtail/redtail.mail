using System;
using System.Collections.Generic;

namespace RedTail.Mail
{
  public interface IMailService
  {
    MailResponse Send(MailRequest request, Boolean stopOnError = true);
		MailResponse Send(MailContent content, MailAddress from, MailPersonalization personalizations, Boolean stopOnError = true);
		MailResponse Send(MailContent content, MailAddress from, List<MailPersonalization> personalizations, Boolean stopOnError = true);
		MailResponse Send(MailContent content, MailAddress from, String subject, MailAddress to, Boolean stopOnError = true);
		MailResponse Send(MailContent content, MailAddress from, String subject, List<MailAddress> to, Boolean stopOnError = true);
		MailResponse Send(String body, String fromAddress, String subject, String toAddress, Boolean stopOnError = true);
		MailResponse Send(String body, String fromAddress, String fromDisplayName, String subject, String toAddress, Boolean stopOnError = true);
		MailResponse Send(String body, String fromAddress, String fromDisplayName, String subject, String toAddress, String toDisplayName, Boolean stopOnError = true);
  }
}
