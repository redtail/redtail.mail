using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using SendGrid;
using SendGrid.Helpers.Mail;

namespace RedTail.Mail
{
  public class MailHandler
    : BaseHandler<MailRequest, MailResponse>
  {
    public MailHandler(MailRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private HttpContent Body;
    private HttpResponseHeaders Headers;
    private SendGridMessage SendGridMessage;
    private HttpStatusCode StatusCode;

    protected override void Command(MailRequest request)
    {
      if (MailConfiguration.Settings.Client == MailClient.SendGrid)
      {
        this.SendBySendGrid(request).Wait();
      }
      else
      {
        this.SendBySmtpClient();
      }
    }

		protected override void Done(MailResponse response)
		{
      response.Body = this.Body;
      response.Headers = this.Headers;
      response.StatusCode = this.StatusCode;

      if (MailConfiguration.Settings.Client == MailClient.SendGrid)
      {
        response.SendGrid.Message = this.SendGridMessage;
      }
      else
      {

      }
		}

		protected override void Error(MailResponse response)
		{

		}

    private async Task SendBySendGrid(MailRequest request)
    {
      SendGridClient client = new SendGridClient(MailConfiguration.Settings.SendGrid.ApiKey);

      this.SendGridMessage = new SendGridMessage()
      {
          From = request.From.ToSendGridEmailAddress(),
          PlainTextContent = request.Content.Text,
          HtmlContent = request.Content.Html
      };

      this.SendGridMessage.Personalizations = new List<Personalization>();
      foreach (MailPersonalization itmMailPersonalization in request.Personalizations)
        this.SendGridMessage.Personalizations.Add(itmMailPersonalization.ToSendGridPersonalization());

      Response response = await client.SendEmailAsync(this.SendGridMessage);
      this.Body = response.Body;
      this.Headers = response.Headers;
      this.StatusCode = response.StatusCode;
    }

    private void SendBySmtpClient()
    {
      throw new NotImplementedException();
    }
  }
}
