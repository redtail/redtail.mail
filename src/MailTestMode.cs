using System;

namespace RedTail.Mail
{
  public class MailTestMode
  {
    public String EmailAddress { get; set; }
    public Boolean TestMode { get; set; }
  }
}
