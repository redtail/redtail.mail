using System;

namespace RedTail.Mail
{
  public class MailContent
  {
    public String Html { get; set; }
    public String Text { get; set; }
  }
}
