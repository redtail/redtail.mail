using SendGrid.Helpers.Mail;

namespace RedTail.Mail
{
  public class MailSendGridResponse
  {
    public SendGridMessage Message { get; set; }
  }
}
