using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace RedTail.Mail
{
  public class MailResponse
    : BaseResponse
  {
    public HttpContent Body { get; set; }
    public HttpResponseHeaders Headers { get; set; }
    public MailSendGridResponse SendGrid { get; set; } = new MailSendGridResponse();
    public HttpStatusCode StatusCode { get; set; }
  }
}
