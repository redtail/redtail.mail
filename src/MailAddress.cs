using System;

using SendGrid.Helpers.Mail;

namespace RedTail.Mail
{
  public class MailAddress
  {
    public String Address { get; set; }
    public String Name { get; set; }

    public EmailAddress ToSendGridEmailAddress()
    {
      return new EmailAddress(this.Address, this.Name);
    }
  }
}
