using System;

namespace RedTail.Mail
{
  public class MailConfiguration
  {
    public static MailSettings Settings { get; private set; } = new MailSettings();

    public static void Configure(Action<MailSettings> cb)
    {
      cb(Settings);
    }
  }
}
