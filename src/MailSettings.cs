using System;

namespace RedTail.Mail
{
  public class MailSettings
  {
    public MailClient Client { get; set; } = MailClient.SmtpClient;
    public MailSendGridSettings SendGrid { get; set; }
    public MailSmtpSettings Smtp { get; set; }
    public MailTestMode TestMode { get; set; }
  }
}
